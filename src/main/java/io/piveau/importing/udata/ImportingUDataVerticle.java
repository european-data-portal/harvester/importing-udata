package io.piveau.importing.udata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.piveau.importing.response.UDataResponse;
import io.piveau.pipe.connector.PipeContext;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class ImportingUDataVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.importing.udata.queue";

    private WebClient client;

    private Cache<String, JsonObject> cache;

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);
        client = WebClient.create(vertx);

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("udataZones", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                        ResourcePoolsBuilder.newResourcePoolsBuilder().heap(2000, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cache = cacheManager.getCache("udataZones", String.class, JsonObject.class);

        startFuture.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        JsonNode config = pipeContext.getConfig();
        pipeContext.log().info("Import started.");

        String address = config.path("address").asText();
        fetchPage(address + "/api/1/datasets/?page_size=100&page=1", pipeContext, new ArrayList<>());
    }

    private void fetchPage(String address, PipeContext pipeContext, List<String> identifiers) {
        HttpRequest<Buffer> request = client.getAbs(address);
        request.send(ar -> {
            if (ar.succeeded()) {
                UDataResponse response = new UDataResponse(ar.result().bodyAsJsonObject());
                if (response.isSuccess()) {
                    JsonObject content = response.getResult().getContent();
                    JsonArray data = content.getJsonArray("data", new JsonArray());

                    long total = content.getLong("total");
                    data.forEach(object -> forwardDataset((JsonObject) object, pipeContext, total, identifiers));

                    String nextPage = content.getString("next_page");
                    if (nextPage != null && !nextPage.isEmpty()) {
                        fetchPage(nextPage, pipeContext, identifiers);
                    } else {
                        pipeContext.log().info("Import metadata finished");
                        vertx.setTimer(5000, t -> pipeContext.setResult(new JsonArray(identifiers).encodePrettily(), "application/json", new ObjectMapper().createObjectNode().put("content", "identifierList")).forward(client));
                    }
                } else {
                    pipeContext.setFailure(response.getError().getMessage());
                }
            } else {
                pipeContext.setFailure(ar.cause());
            }
        });

    }

    private void forwardDataset(JsonObject dataset, PipeContext pipeContext, long total, List<String> identifiers) {
        String address = pipeContext.getConfig().path("address").asText();
        completeSpatialInfo(address, dataset, ar -> {
            identifiers.add(dataset.getString("slug"));
            ObjectNode dataInfo = new ObjectMapper().createObjectNode()
                    .put("total", total)
                    .put("counter", identifiers.size())
                    .put("identifier", dataset.getString("slug"));
            pipeContext.setResult(dataset.encodePrettily(), "application/json", dataInfo).forward(client);
            if (ar.succeeded()) {
                pipeContext.log().info("Data imported: {}", dataInfo);
            } else {
                pipeContext.log().warn("Data may imported incomplete: {}", dataInfo);
            }
        });
    }

    private void completeSpatialInfo(String address, JsonObject dataset, Handler<AsyncResult<Void>> handler) {
        JsonObject spatial = dataset.getJsonObject("spatial", new JsonObject());
        if (spatial != null) {
            JsonArray zones = spatial.getJsonArray("zones", new JsonArray());
            if (zones == null || zones.isEmpty()) {
                handler.handle(Future.succeededFuture());
            } else {
                List<Future> futures = new ArrayList<>();
                zones.forEach(obj -> {
                    String zone = obj.toString();
                    Future<JsonObject> future = fetchZone(address, zone);
                    futures.add(future);
                });
                CompositeFuture.all(futures).setHandler(fut -> {
                    JsonArray geometries = new JsonArray();
                    futures.forEach(f -> {
                        if (f.succeeded()) {
                            geometries.add((JsonObject)f.result());
                        }
                    });
                    spatial.put("geom", geometries);
                    handler.handle(Future.succeededFuture());
                });
            }
        } else {
            handler.handle(Future.succeededFuture());
        }
    }

    private Future<JsonObject> fetchZone(String address, String zone) {
        Future<JsonObject> future = Future.future();

        if (cache.containsKey(zone)) {
            future.complete(cache.get(zone));
        } else {
            try {
                HttpRequest<Buffer> request = client.getAbs(address + "/api/1/spatial/zones/" + URLEncoder.encode(zone, "UTF-8"));

                request.send(ar -> {
                    if (ar.succeeded()) {
                        JsonObject result = ar.result().bodyAsJsonObject();
                        JsonArray features = result.getJsonArray("features", new JsonArray());
                        if (!features.isEmpty()) {
                            JsonObject geometry = features.getJsonObject(0).getJsonObject("geometry");
                            cache.put(zone, geometry);
                            future.complete(geometry);
                        } else {
                            future.fail("no geometry found");
                        }
                    } else {
                        future.fail(ar.cause());
                    }
                });
            } catch (UnsupportedEncodingException e) {
                future.fail(e);
            }
        }

        return future;
    }

}