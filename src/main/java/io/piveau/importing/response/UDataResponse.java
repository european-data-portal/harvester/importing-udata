package io.piveau.importing.response;

import io.vertx.core.json.JsonObject;

public class UDataResponse extends HttpResponse<JsonObject> {

	public UDataResponse(JsonObject content) {
		super(content);
	}

	@Override
	public boolean isError() {
		return content == null || !content.containsKey("data");
	}

	@Override
	public boolean isSuccess() {
		return !isError();
	}

	@Override
	public UDataError getError() {
		return isError() ? new UDataError(content.getJsonObject("error")) : null;
	}

	@Override
	public UDataResult getResult() {
		return isSuccess() ? new UDataResult(content) : null;
	}
	
}
