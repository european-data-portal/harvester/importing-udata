package io.piveau.importing.response;

import io.vertx.core.json.JsonObject;

public class UDataResult extends HttpResult<JsonObject> {

	public UDataResult(JsonObject result) {
		super(result);
	}

	@Override
	public JsonObject getContent() {
		return result;
	}
	
}
